/**
    Vue.js Code
**/

//creates the cards for each brand logo
Vue.component('brands-list', {
  props: ['brand'],
  template: `<div class='card' :data-brand='brand.name' :data-id='brand.id'>
              <img class='card-img-top' :src="'imgs/logos/'+brand.name +'.png'" :alt="brand.name +' logo'">
            </div>`
});

//componet that creates carousel items for a specific brand's model
var brandComponent = Vue.extend({
  props: ['modelphoto', 'index', 'detailsbrand'],
  template: `
  <div v-if="detailsbrand.show" class="carousel-item" v-bind:class="[{ active: index === 0 }]">
    <img class="d-block w-100 img-fluid" :src="'imgs/vehicles/'+detailsbrand.name+'/'+modelphoto" :alt="detailsbrand.name">
  </div>`
});

//descripation componet floating over the slide items
Vue.component('model-desc', {
  props: ['detailsbrand'],
  template: `
  <div class="carousel-caption-modified d-none d-md-block">
    <h5>{{detailsbrand.model}}</h5>
    <table>
      <thead>
        <th>Year</th>
        <th>Engine</th>
        <th>Horse Power (HP)</th>
        <th>0-60 MPH</th>
      </thead>
      <tbody>
        <tr>
          <td><p>{{detailsbrand.year}}</p></td>
          <td><p>{{detailsbrand.engine}}</p></td>
          <td><p>{{detailsbrand.horsePower}}</p></td>
          <td><p>{{detailsbrand.north260}}</p></td>
        </tr>
      </tbody>
    </table>
    <!--<p>{{detailsbrand.desc}}</p>-->
  </div>`
});

Vue.component('tab-AMG', brandComponent)
Vue.component('tab-Audi', brandComponent)
Vue.component('tab-Porsche', brandComponent)

var carApp = new Vue({
  el: '#vroom',
  data: {
    currentBrandName: {
      name: 'AMG',
      id: 0
    },
    brandsList: [{
        id: 0,
        name: 'AMG',
        model: 'SLS Black Edition',
        year: '2014',
        engine: '6.3-litre V8',
        north260: '3.2',
        horsePower: '622',
        show: true,
        desc: `Inspired by the SLS AMG GT3 race car, the fifth Black Series model from Mercedes-AMG boasts a fascinating mix of breath-taking design, outstanding driving dynamics and uncompromising lightweight construction in accordance with the "AMG Lightweight Performance" strategy.`,
        gallery: ['front.jpg', 'engine.jpg', 'rear.jpg']
      },
      {
        id: 1,
        name: 'Audi',
        model: 'R8 V10 Plus',
        year: '2016',
        engine: '5.2-litre V10',
        north260: '2.9',
        show: false,
        horsePower: '610',
        desc: `No model with the four rings is closer to motorsport, none is more striking and more dynamic: at the 2015 Geneva Motor Show, Audi is presenting the second generation of its high-performance R8 sports car.`,
        gallery: ['front.jpg', 'cockpit.jpg', 'rear.jpg']
      },
      {
        id: 2,
        name: 'Porsche',
        model: '911 R',
        show: false,
        year: '2017',
        engine: '4.0-litre flat-six',
        north260: '3.7',
        horsePower: '500',
        desc: `With its new Porsche 911 R, Porsche unveiled a puristic sports car in classical design at the 2016 Geneva International Motor Show. Its 368 kW (500 hp) four-litre naturally aspirated flat engine and six-speed manual sports transmission places the 911 R firmly in the tradition of its historic role model: a road-homologated racing car from 1967. `,
        gallery: ['front.jpg', 'cockpit.jpg', 'rear.jpg']
      }
    ],
    animation: {
      pushStart: true,
      alert: true,
    },
  },
  methods: {
    start: function(event) {
      //should be true
      // this.animation.pushStart = true
      this.$data.animation.pushStart = false
      var engineStart = new Audio('sounds/engine-start.mp3')
      engineStart.play()


      //first approch but not recommended
      //var self = this
      // setTimeout(function(animation) {
      //   self.animation.alert = false
      // }, 5000);

      //using arrow approch ES6
      setTimeout(() => {
        this.animation.alert = false
      }, 5000);
      // alert(this.animation.pushStart)

    },
    showModel: function(event) {
      if (!this.animation.pushStart) {
        var brand = event.currentTarget.getAttribute('data-brand')
        var brandId = event.currentTarget.getAttribute('data-id')
        this.currentBrandName.name = brand
        this.currentBrandName.id = brandId
        this.brandsList[brandId].show = true
      }
      // console.log(this.currentBrandName)
    }
  },
  computed: {
    currentBrandTab: function() {
      // alert(this.currentBrandName+" <-C")
      return "tab-" + this.currentBrandName.name
    },
    currentBrand: function() {
      return this.currentBrandName
    }
  },
  components: {
    'tab-AMG': brandComponent,
    'tab-Audi': brandComponent,
    'tab-Porsche': brandComponent
  }
})

/**
  / Vue.js Code
**/
